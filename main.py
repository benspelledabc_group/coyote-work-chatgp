# this repo helped A LOT
# https://github.com/raspberrypi/picamera2

# we should also write out to a drive.. not the micro sd card...



#!/usr/bin/python3

import RPi.GPIO as GPIO
import time, json
from datetime import datetime

import libcamera
from picamera2 import Picamera2, Preview
from libcamera import controls

import boto3, os, glob

import psycopg2
from psycopg2.extras import Json

GPIO.setmode(GPIO.BCM)
motion_sensor_pin = 24  # Change this to the GPIO pin you connected the motion sensor to
GPIO.setup(motion_sensor_pin, GPIO.IN)

picam2 = Picamera2()

#picam2.set_controls({"AfMode": controls.AfModeEnum.Continuous})
picam2.set_controls({"AfMode": controls.AfModeEnum.Continuous, "AfSpeed": controls.AfSpeedEnum.Fast})

from dotenv import load_dotenv
load_dotenv()
PG_HOST = os.getenv("PG_HOST")
PG_PORT = os.getenv("PG_PORT")
PG_USER = os.getenv("PG_USER")
PG_PASS = os.getenv("PG_PASS")
PG_DB = os.getenv("PG_DB")


# FLIP IT
# my_config = picam2.create_still_configuration(main={"size": (640, 480)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1), display="lores")
# my_config = picam2.create_still_configuration(main={"size": (1920, 1080)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1), display="lores")
# my_config = picam2.create_still_configuration(main={"size": (1920, 1080)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1), display="main")
# my_config = picam2.create_still_configuration(main={"size": (1920, 1080)}, lores={"size": (1920, 1080)}, transform=libcamera.Transform(vflip=1), display="main")

# https://raspberrypi.stackexchange.com/questions/58871/pi-camera-v2-fast-full-sensor-capture-mode-with-downsampling
#my_config = picam2.create_still_configuration(main={"size": (3280, 2464)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1), display="lores")
#my_config = picam2.create_still_configuration(main={"size": (1640, 922)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1), display="lores")
#my_config = picam2.create_still_configuration(main={"size": (1280, 720)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1), display="lores")
#my_config = picam2.create_still_configuration(main={"size": (1920, 1060)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1), display="lores")
#my_config = picam2.create_still_configuration(main={"size": (640, 480)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1), display="lores")


#4608x2592
#my_config = picam2.create_still_configuration(main={"size": (4608, 2592)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1), display="lores")
#2304x1296
#my_config = picam2.create_still_configuration(main={"size": (2304, 1296)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1), display="main")


# Working with good resoltuions
#my_config = picam2.create_still_configuration(main={"size": (1600, 1200)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1), display="main")
#my_config = picam2.create_still_configuration(main={"size": (2560, 1600)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1), display="main")
#my_config = picam2.create_still_configuration(main={"size": (3860, 2160)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1), display="main")



my_config = picam2.create_still_configuration(main={"size": (3860, 2160)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1,hflip=1), display="main")




picam2.configure(my_config)
picam2.start()
#success = picam2.autofocus_cycle()




# clean up the old .jpg first..
print("Doing a little housecleaning prepwork.")
for f in glob.glob("*.jpg"):
    os.remove(f)


def push_to_db(input):
    print(input)

    # last known working good
    formated_input = '{"photo_path": "' + input + '"}'

    # lets put approved_status in the JSON object
    # formated_input = '{"photo_path": "' + input + '", "approval_status": "pending"}'


    formated_input = json.loads(formated_input)
    input = formated_input


    try:
       print("Making db connection...")

       # Establishing the connection
       conn = psycopg2.connect(
           database=PG_DB,
           user=PG_USER,
           password=PG_PASS,
           host=PG_HOST,
           port=PG_PORT,
       )

       print("Made db connection...")

       # Setting auto commit false
       conn.autocommit = True

       # Creating a cursor object using the cursor() method
       cursor = conn.cursor()

       print("Pushing data...")
       # Preparing SQL queries to INSERT a record into the database.
       cursor.execute(
           "INSERT INTO sensor_posts(json_data, date, approved) VALUES (%s, current_timestamp, false)",
           [Json(input)],
       )

       # Commit your changes in the database
       conn.commit()
       print("Records inserted...")
    except Exception as ex:
       print(f"Database insert failed: {ex}")
    finally:
       # Closing the connection
       conn.close()
       print("DB connection closed...")








def upload_pic_to_aws(input_pic):
    try:
        bucket = "awsunicorns"
        aws_base_path = "public/coyote_work/pics"
        path_filtering = datetime.today().strftime("%Y/%m/%d")        
        #dynamic_filetered_path = f"{aws_base_path}/{path_filtering}"
        folder = f"{aws_base_path}/{path_filtering}"

        s3 = boto3.resource("s3")
        data = open(input_pic, "rb")
        dest = "{folder}/{inputfile}".format(inputfile=input_pic, folder=folder)
        s3.Bucket(bucket).put_object(Key=dest, Body=data)
        full_dest = "https://{bucket}.{base_url}/{dest}".format(
            base_url="s3.amazonaws.com", dest=dest, bucket=bucket
        )
        print(f"pic uploaded to {full_dest}")

        return full_dest
    except Exception as ex:
        print(ex.message)
        return "http://google.com"




def do_camera_work():
    try:
       import uuid
       uuid = uuid.uuid4().hex

       dt = datetime.now()
       readable_date = dt.strftime('%y-%m-%d')
       unique_imagename = readable_date + "_" + uuid + ".jpg"

       #success = picam2.autofocus_cycle()
       request = picam2.capture_request()

       #print("focus 1 start")
       #success = picam2.autofocus_cycle()
       #print("focus 1 stop")

       #print("focus 2 start")
       #success2 = picam2.autofocus_cycle()
       #print("focus 2 stop")



       request.save("main", unique_imagename)
       request.release()
       #print("Still image captured! ({0})".format(unique_imagename))

       # upload
       aws_path = upload_pic_to_aws(unique_imagename)
       push_to_db(aws_path)


       # clean up image
       #print("About to delete {0}".format(unique_imagename))
       os.remove(unique_imagename)
       print("{0} removed for cleanup...".format(unique_imagename))
    except Exception as ex:
       print("Exception: {0}".format(ex))



def motion_detected(channel):
    print("Motion detected!")
    # Add your code here to handle the motion detection event
    do_camera_work()


GPIO.add_event_detect(motion_sensor_pin, GPIO.RISING, callback=motion_detected)


while True:
    time.sleep(1)  # Sleep to reduce CPU usage


GPIO.cleanup()
