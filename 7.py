# this repo helped A LOT
# https://github.com/raspberrypi/picamera2

# we should also write out to a drive.. not the micro sd card...



#!/usr/bin/python3

import RPi.GPIO as GPIO
import time, json
from datetime import datetime

import libcamera
from picamera2 import Picamera2, Preview

import boto3, os, glob

import psycopg2
from psycopg2.extras import Json

GPIO.setmode(GPIO.BCM)
motion_sensor_pin = 24  # Change this to the GPIO pin you connected the motion sensor to
GPIO.setup(motion_sensor_pin, GPIO.IN)

picam2 = Picamera2()

from dotenv import load_dotenv
load_dotenv()
PG_HOST = os.getenv("PG_HOST")
PG_PORT = os.getenv("PG_PORT")
PG_USER = os.getenv("PG_USER")
PG_PASS = os.getenv("PG_PASS")
PG_DB = os.getenv("PG_DB")


# FLIP IT
#my_config = picam2.create_still_configuration(main={"size": (1920, 1080)}, lores={"size": (640, 480)}, transform=libcamera.Transform(vflip=1), display="lores")
my_config = picam2.create_still_configuration(main={"size": (4608,2592)}, lores={"size": (4608, 2592)}, transform=libcamera.Transform(vflip=1), display="lores")


picam2.configure(my_config)
picam2.start()

# clean up the old .jpg first..
print("Doing a little housecleaning prepwork.")
for f in glob.glob("*.jpg"):
    os.remove(f)


def push_to_db(input):
    print(input)

    # exmaple, simplified
    #{"photo_path": "https://picsum.photos/1280/1024"}

    #formated_input = '{"photo_path": "https://awsunicorns.s3.amazonaws.com/public/coyote_work/pics/2023/07/16/23-07-16_10bbfb069554404b897c22a667f84d61.jpg"}'
    #formated_input = "{\"photo_path\": \"{0}\"}".format(input)
    #formated_input = f"{'photo_path': '{0}'}".format(input)
    #formated_input = "{'photo_path': '{0}'}".format(input)
    #formated_input = "{\"photo_path\": \"{0}\"}".format(input)
    #formated_input = "photo_path: {0}".format(input)

    formated_input = '{"photo_path": "https://picsum.photos/200/300"}'
    formated_input = json.loads(formated_input)
    input = formated_input


    try:
       print("Making db connection...")

       print("PG_DB: {0}".format(PG_DB))

       # Establishing the connection
       conn = psycopg2.connect(
           database=PG_DB,
           user=PG_USER,
           password=PG_PASS,
           host=PG_HOST,
           port=PG_PORT,
       )

       print("Made db connection...")

       # Setting auto commit false
       conn.autocommit = True

       # Creating a cursor object using the cursor() method
       cursor = conn.cursor()

       print("Pushing data...")
       # Preparing SQL queries to INSERT a record into the database.
       cursor.execute(
           "INSERT INTO sensor_posts(json_data, date) VALUES (%s, current_timestamp)",
           [Json(input)],
       )

       # Commit your changes in the database
       conn.commit()
       print("Records inserted...")
    except Exception as ex:
       print(f"Database insert failed: {ex}")
    finally:
       # Closing the connection
       conn.close()
       print("DB connection closed...")








def upload_pic_to_aws(input_pic):
    try:
        bucket = "awsunicorns"
        aws_base_path = "public/coyote_work/pics"
        path_filtering = datetime.today().strftime("%Y/%m/%d")        
        #dynamic_filetered_path = f"{aws_base_path}/{path_filtering}"
        folder = f"{aws_base_path}/{path_filtering}"

        s3 = boto3.resource("s3")
        data = open(input_pic, "rb")
        dest = "{folder}/{inputfile}".format(inputfile=input_pic, folder=folder)
        s3.Bucket(bucket).put_object(Key=dest, Body=data)
        full_dest = "https://{bucket}.{base_url}/{dest}".format(
            base_url="s3.amazonaws.com", dest=dest, bucket=bucket
        )
        print(f"pic uploaded to {full_dest}")

        return full_dest
    except Exception as ex:
        print(ex.message)
        return "http://google.com"




def do_camera_work():
    try:
       import uuid
       uuid = uuid.uuid4().hex

       dt = datetime.now()
       readable_date = dt.strftime('%y-%m-%d')
       unique_imagename = readable_date + "_" + uuid + ".jpg"

       request = picam2.capture_request()

       request.save("main", unique_imagename)
       request.release()
       #print("Still image captured! ({0})".format(unique_imagename))

       # upload
       aws_path = upload_pic_to_aws(unique_imagename)
       push_to_db(aws_path)


       # clean up image
       #print("About to delete {0}".format(unique_imagename))
       os.remove(unique_imagename)
       print("{0} removed for cleanup...".format(unique_imagename))
    except Exception as ex:
       print("Exception: {0}".format(ex.message))



def motion_detected(channel):
    print("Motion detected!")
    # Add your code here to handle the motion detection event
    do_camera_work()


GPIO.add_event_detect(motion_sensor_pin, GPIO.RISING, callback=motion_detected)


while True:
    time.sleep(1)  # Sleep to reduce CPU usage


GPIO.cleanup()
